/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { OnlyNumericDirective } from './OnlyNumeric.directive';

describe('Directive: OnlyNumeric', () => {
  it('should create an instance', () => {
    const directive = new OnlyNumericDirective();
    expect(directive).toBeTruthy();
  });
});
