import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BaseServiceService } from "../base-service.service";
import { HttpClient, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class TransactionService {
  path: string;
  constructor(private base: BaseServiceService, private http: HttpClient) {
    this.path = this.base.path + "transaction/";
  }

  GetTransactionTemplate(id: any): Observable<any> {
    let param = new HttpParams().set("personID", id);
    return this.http.post<any>(this.path + "getTransactionTemplate", param);
  }
  GetAllTransactionTemplate(id:any): Observable<any> {
    let param = new HttpParams().set("personID", id);
    return this.http.post<any>(this.path + "getAllTransactionTemplate", param);
  }

  GetTermTransactionsByPerson(id: any): Observable<any> {
    let param = new HttpParams().set("personID", id);
    return this.http.post<any>(this.path + "getTermTransactions", param);
  }
  GetTransactionByTemplate(id:any): Observable<any>{
    let param = new HttpParams().set("templateID", id);
    return this.http.post<any>(this.path + "getTransactionByTemplate", param);
  }

  GetTypeTransaction(): Observable<any> {
    return this.http.get<any>(this.path + "getAllType");
  }

  GetTypeTransactionByID(id: string): Observable<any> {
    let param = new HttpParams().set("id", id);
    return this.http.post(this.path + "getType", param);
  }

  GetTypeTransactionDayRange(): Observable<any> {
    return this.http.get<any>(this.path + "getAllDayRange");
  }

  GetTypeTransactionDayRangeByID(id: string): Observable<any> {
    let param = new HttpParams().set("id", id);
    return this.http.post(this.path + "getDayRange", param);
  }

  GetTypePayments():Observable<any>{
    return this.http.get<any>(this.path + "getTypePayments");
  }

  GetPersonTransactions(id:string, categoryID:any):Observable<any>{
    let param = new HttpParams().set("personID", id).set("categoryID", categoryID);
    return this.http.post(this.path + "getPersonTransactions", param);
  }
}
