import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AlertifyService } from "../alertify-service/alertify.service";
import { Router } from "@angular/router";
import { BaseServiceService } from "../base-service.service";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  path:string;

  constructor(private http : HttpClient, private alert:AlertifyService, private route:Router, private base:BaseServiceService) {
    this.path = this.base.path + "auth/";
  }

  Login(loginUser: any) {
    this.http.post(this.path + "login", loginUser).subscribe(data=>{
      debugger;
      if(data != null){
        sessionStorage.setItem("ONLINEUSER", JSON.stringify(data));
        this.alert.success("Başarılı");
        this.route.navigateByUrl("/main");
      }else{
        this.alert.error("Başarısız");
      }
    })
  }
}
