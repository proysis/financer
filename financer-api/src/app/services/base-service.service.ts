import { Injectable } from "@angular/core";
import { AlertifyService } from "./alertify-service/alertify.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
@Injectable()
export class BaseServiceService {
  action: string = "";
  controller: string = "";

  constructor(protected http: HttpClient, protected alert: AlertifyService) {}

  localPath = "http://localhost:52165/api/"
  serverPath = "http://207.154.248.28:443/api/";
  path = this.serverPath;

  Update(value: any) {
    return this.http.post(this.path + "base/updateEntity", value)
  }

  Delete(value: any) {
    return this.http.post(this.path + "base/deleteEntity", value);
  }
}
