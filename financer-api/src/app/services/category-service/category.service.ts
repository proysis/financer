import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BaseServiceService } from "../base-service.service";
import { HttpClient, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class CategoryService {
  constructor(private http:HttpClient, private base: BaseServiceService) {}

  path = this.base.path + "category/";

  GetCategoriesByPersonID(personID: string): Observable<any> {
    let param = new HttpParams().set("personID", personID)
    return this.http.post(this.path + "categoryByPersonID", param);
  }

  Delete(parentID:string): Observable<any>{
    let param = new HttpParams().set("parentID", parentID)
    return this.http.post(this.path + "delete", param);
  }

  GetCategoriesByCategoryID(personID: string, categoryID: any): Observable<any> {
    let param = new HttpParams().set("personID", personID).set("categoryID",categoryID)
    return this.http.post(this.path + "categoryByCategoryID", param);
  }
}
