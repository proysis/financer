import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AlertifyService } from "../alertify-service/alertify.service";
import { BaseServiceService } from "../base-service.service";

@Injectable({
  providedIn: "root"
})
@Injectable()
export class UserService {
  constructor(protected http: HttpClient, protected alert: AlertifyService, public base : BaseServiceService) {
    this.base.action = "values";
    
  }

  public GetUser(): Observable<any> {
    return this.http.get<any>(this.base.path + this.base.action);
  }

  public GetUserByID(id: string): Observable<any> {
    return this.http.get<any>(this.base.path + this.base.action+ "/" + id);
  }


}
