import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { LoginService } from "../services/login-service/login.service";
import { Router } from "@angular/router";
import "../../assets/pages/scripts/login-5.min.js"
import { BaseServiceService } from "../services/base-service.service";
import { AlertifyService } from "../services/alertify-service/alertify.service";
import { DatePipe } from "@angular/common";

declare var Login : any;



@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginUser: any = {};
  registerUser:any = {};
  constructor(private loginService: LoginService, private route: Router, private base:BaseServiceService, private alertify:AlertifyService, private datePipe: DatePipe) {}
  @ViewChild('register') register:ElementRef;
  @ViewChild('login') login:ElementRef;
  @ViewChild('rPassword') rPassword:ElementRef;
 

  ngOnInit() {
    Login.init();
    if (sessionStorage.getItem("ONLINEUSER") != null) {
      this.route.navigateByUrl("/main");
    } else {
      
    }
    this.registerUser.className="PERSON";
  }

  SignIn() {
    debugger;
    this.loginService.Login(this.loginUser);
  }

  ShowRegister(){
    this.register.nativeElement.style = "display:block;";
    this.login.nativeElement.style = "display:none;";
  }
  ShowLogin(){
    this.register.nativeElement.style = "display:none;";
    this.login.nativeElement.style = "display:block;";
  }

  SignUp(){
    if(this.rPassword.nativeElement.value == this.registerUser.password){
      this.registerUser.createdate = this.datePipe.transform(new Date(),"MM/dd/yyyy");
      this.base.Update(this.registerUser).subscribe(data=>{
        this.alertify.success("Registration successfully");
      });
    }else{
      this.alertify.error("Please confirm password.");
    }
    
  }
  
}
