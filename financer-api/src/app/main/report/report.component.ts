import { Component, OnInit } from "@angular/core";
import "../../../assets/custom/customChart.js";
import { TransactionService } from "src/app/services/transaction-service/transaction.service.js";
import { BaseServiceService } from "src/app/services/base-service.service.js";
import { forEach } from "@angular/router/src/utils/collection";
import * as Enumerable from "linq-es2015";
import { DrpCategoryComponent } from "./drp-category/drp-category.component.js";

declare var CustomCharts: any;

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"]
})
export class ReportComponent implements OnInit {
  constructor(
    private base: BaseServiceService,
    private transactionService: TransactionService
  ) {}

  personTransactions: any = [];
  onlineUser: any = {};
  totalIncome: any = 0;
  totalExpense: any = 0;
  currentDeficitThisMonth: any = 0;
  currentDeficit: any = 0;
  ngOnInit() {
    this.onlineUser = JSON.parse(sessionStorage.getItem("ONLINEUSER"));
    this.fillReport(null);
  }

  fillReport(categoryID: any) {
    let date: Date = new Date();
    this.totalIncome = 0;
    this.totalExpense = 0;
    this.currentDeficitThisMonth = 0;
    this.currentDeficit = 0;
    this.transactionService
      .GetPersonTransactions(this.onlineUser.id, categoryID)
      .subscribe(data => {
        this.personTransactions = data;

        let chartDataIncome = [];
        let chartDataExpense = [];
        this.personTransactions.forEach(element => {
          if (element.typetransactionid == 1) {
            chartDataIncome.push([
              this.GetMonthName(element.month) + " " + element.year,
              element.totalamountincome
            ]);
            this.totalIncome += element.totalamountincome;
            if (
              element.month == date.getMonth() + 1 &&
              element.year == date.getFullYear()
            ) {
              this.currentDeficitThisMonth += element.totalamountincome;
            }
          } else if (element.typetransactionid == 2) {
            chartDataExpense.push([
              this.GetMonthName(element.month) + " " + element.year,
              element.totalamountexpense
            ]);
            this.totalExpense += element.totalamountexpense;
            if (
              element.month == date.getMonth() + 1 &&
              element.year == date.getFullYear()
            ) {
              this.currentDeficitThisMonth -= element.totalamountexpense;
            }
          }
        });

        this.currentDeficit = this.totalIncome - this.totalExpense;

        CustomCharts.initCharts(chartDataIncome, chartDataExpense);
      });
  }

  GetMonthName(monthNum: number): string {
    let monthName: string;

    switch (monthNum) {
      case 1:
        monthName = "JAN";
        break;
      case 2:
        monthName = "FEB";
        break;
      case 3:
        monthName = "MAR";
        break;
      case 4:
        monthName = "APR";
        break;
      case 5:
        monthName = "MAY";
        break;
      case 6:
        monthName = "JUN";
        break;
      case 7:
        monthName = "JUL";
        break;
      case 8:
        monthName = "AUG";
        break;
      case 9:
        monthName = "SEP";
        break;
      case 10:
        monthName = "OCT";
        break;
      case 11:
        monthName = "NOV";
        break;
      case 12:
        monthName = "DEC";
        break;
    }

    return monthName;
  }
}
