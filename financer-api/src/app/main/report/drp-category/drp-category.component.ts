import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { CategoryService } from "src/app/services/category-service/category.service";

@Component({
  selector: "app-drp-category",
  templateUrl: "./drp-category.component.html",
  styleUrls: ["./drp-category.component.css"]
})
export class DrpCategoryComponent implements OnInit {
  constructor(private categoryService: CategoryService) {}

  @Input()
  parentCategoryID: any;
  @Input()
  childCategories: any[] = [];

  @Output()
  fillReport = new EventEmitter<string>();

  onlineuser: any;
  id: any;
  ngOnInit() {
    this.onlineuser = JSON.parse(sessionStorage.getItem("ONLINEUSER"));
    if (this.childCategories.length == 0) {
      this.categoryService
        .GetCategoriesByCategoryID(this.onlineuser.id, null)
        .subscribe(data => {
          debugger;
          this.childCategories = data;
        });
    }
  }

  Change(id:any){
    debugger;
      this.categoryService
        .GetCategoriesByCategoryID(this.onlineuser.id, id)
        .subscribe(data => {
          debugger;
          this.childCategories = data;
        });
  }

  GetChild(id: any): any {
    if (id != undefined && id != null) {
      id = id == "0" ? null : id;
      sessionStorage.setItem("CATEGORYID", id);
      this.categoryService
        .GetCategoriesByCategoryID(this.onlineuser.id, id)
        .subscribe(data => {
          this.Change(id);
          this.fillReport.emit(id);
          return data;
        });
    } else {
      return null;
    }
  }
}
