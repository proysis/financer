import { Component, OnInit, Input } from "@angular/core";
import { MyTreeComponent } from "../my-tree.component";
import { MatDialog } from "@angular/material";
import { UpdateTreeComponent } from "../update-tree/update-tree.component";
import { CategoryService } from "src/app/services/category-service/category.service";
import { AlertifyService } from "src/app/services/alertify-service/alertify.service";
import { UpdateTransactionTemplateComponent } from "../../transaction/update-transaction-template/update-transaction-template.component";
import { TransactionService } from "src/app/services/transaction-service/transaction.service";
import { Jsonp } from "@angular/http";
import { FilterByIDPipe } from "src/app/pipes/filterByID.pipe";
import { UpdateTransactionComponent } from "../../transaction/update-transaction/update-transaction.component";

@Component({
  selector: "app-tree-view",
  templateUrl: "./tree-view.component.html",
  styleUrls: ["./tree-view.component.css"]
})
export class TreeViewComponent implements OnInit {
  @Input()
  categories: any[] = [];
  @Input()
  parentIndex: string = "0";

  constructor(
    private parentCom: MyTreeComponent,
    private dialog: MatDialog,
    private categoryService: CategoryService,
    private alertify: AlertifyService,
    private myTreeCom: MyTreeComponent
  ) {}

  ngOnInit() {}

  HasCategoryChild(id: string) {
    return (
      this.categories.find(x => x.id == id).children.length ||
      this.myTreeCom.transactionTemplates.filter(x => x.categoryid == id).length
    );
  }

  HasTermTransaction(id:string){
    return this.myTreeCom.termTransactions.filter(x=>x.transactiontemplateid == id).length;
  }

  Collapse(event: any, parentid) {
    let typeOfButton = event.target.getAttribute("data-action");
    if (typeOfButton == "collapse") {
      debugger;
      document
        .getElementById("btnCollapse-" + parentid)
        .setAttribute("style", "display:none");
      document
        .getElementById("btnExpand-" + parentid)
        .setAttribute("style", "display:block");
      var com = document.getElementById("com-" + parentid);
      if (com !== undefined && com !== null) {
        com.setAttribute("style", "display:none");
      }
      var ol = document.getElementById("ol-" + parentid);
      if (ol !== undefined && ol !== null) {
        ol.setAttribute("style", "display:none");
      }
    } else {
      debugger;
      document
        .getElementById("btnCollapse-" + parentid)
        .setAttribute("style", "display:block");
      document
        .getElementById("btnExpand-" + parentid)
        .setAttribute("style", "display:none");
      var com = document.getElementById("com-" + parentid);
      if (com !== undefined && com !== null) {
        com.setAttribute("style", "display:block");
      }
      var ol = document.getElementById("ol-" + parentid);
      if (ol !== undefined && ol !== null) {
        ol.setAttribute("style", "display:block");
      }
    }
  }

  public ShowDialog(preCategoryInfo: any) {
    let dialogRef = this.dialog.open(UpdateTreeComponent, {
      height: "600px",
      width: "700px",
      data: preCategoryInfo
    });
    dialogRef.componentInstance.lblOperationName = "New Category";
    dialogRef.afterClosed().subscribe(result => {
      this.parentCom.FillCategories();
    });
  }

  public ShowTransactionTemplateDialog(preCategoryInfo: any) {
    let dialogRef = this.dialog.open(UpdateTransactionTemplateComponent, {
      height: "600px",
      width: "700px",
      data: preCategoryInfo
    });
    dialogRef.componentInstance.lblOperationName = "New Transaction Template";
    dialogRef.afterClosed().subscribe(result => {
      this.parentCom.FillCategories();
    });
  }

  public ShowTransactionDialog(data: any) {
    let dialogRef = this.dialog.open(UpdateTransactionComponent, {
      height: "600px",
      width: "700px",
      data: data
    });
    dialogRef.componentInstance.lblOperationName = (data.transaction === undefined ? "New ":"Update ") + "Transaction";
    dialogRef.afterClosed().subscribe(result => {
      this.parentCom.FillCategories();
    });
  }

  Delete(id: string) {
    this.categoryService.Delete(id).subscribe(
      success => {
        this.alertify.success("Silme işlemi başarılı");
        this.parentCom.FillCategories();
      },
      err => {
        this.alertify.error("Silme işlemi başarısız");
      }
    );
  }
}
