import { Component, OnInit } from "@angular/core";
import { CategoryService } from "src/app/services/category-service/category.service.js";
import { MatDialog } from "@angular/material";
import { BaseServiceService } from "src/app/services/base-service.service.js";
import { TransactionService } from "src/app/services/transaction-service/transaction.service";

@Component({
  selector: "app-my-tree",
  templateUrl: "./my-tree.component.html",
  styleUrls: ["./my-tree.component.css"]
})
export class MyTreeComponent implements OnInit {
  constructor(
    private category: CategoryService,
    private dialog: MatDialog,
    private base: BaseServiceService,
    private transactionService: TransactionService
  ) {}

  categories: any[] = [];
  onlineuser: any;
  categoryList: any[] = [];
  transactionTemplates: any[] = [];
  termTransactions: any[] = [];
  
  ngOnInit() {
    this.onlineuser = JSON.parse(sessionStorage.getItem("ONLINEUSER"));
    
    this.FillCategories();
  }

  public FillCategories() {
    debugger;
    this.transactionService.GetTransactionTemplate(this.onlineuser.id).subscribe(data => {
      this.transactionTemplates = data;
    });

    this.transactionService.GetTermTransactionsByPerson(this.onlineuser.id).subscribe(data => {
      this.termTransactions = data;
    });
    
    this.categoryList.splice(0,this.categoryList.length);
    this.category
      .GetCategoriesByPersonID(this.onlineuser.id)
      .subscribe(data => {
        this.categories = data;
        let parents = this.categories.filter(x => x.categoryid == null);
        parents.forEach(element => {
          element.children = this.GetChildCategories(element.id);;
          this.categoryList.push(element);
        });
        
      });
  }

  GetChildCategories(id: string): any {
    let childcategories: any[] = [];
    let i: number = 0;

    childcategories = this.categories.filter(x=>x.categoryid == id);
    if(childcategories.length > 0){
      childcategories.forEach(element =>{
        let index = this.categories.indexOf(element);
        this.categories.splice(index, 1);
        element.children = this.GetChildCategories(element.id);
      });
    }
    return childcategories;
  }
}
