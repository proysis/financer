import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-update-tree",
  templateUrl: "./update-tree.component.html",
  styleUrls: ["./update-tree.component.css"]
})
export class UpdateTreeComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  category: any = {};
  lblOperationName: string = "";

  ngOnInit() {
    debugger;
    
    if (this.data.id != null || this.data.id !== undefined) {
      this.category = this.data;
    } else {
      this.category.categoryid = this.data.categoryid;
      this.category.personid = this.data.personid;
    }
    this.category.className = "CATEGORY";
  }
}
