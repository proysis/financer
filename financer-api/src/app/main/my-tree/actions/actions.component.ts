import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UpdateTreeComponent } from '../update-tree/update-tree.component';
import { MyTreeComponent } from '../my-tree.component';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {

  constructor(private dialog: MatDialog, private myTree:MyTreeComponent) { }
  onlineuser: any;

  ngOnInit() {
    this.onlineuser = JSON.parse(sessionStorage.getItem("ONLINEUSER"));
  }

  public ShowDialog(preCategoryInfo:any) {
    debugger;
     let dialogRef = this.dialog.open(UpdateTreeComponent,{
       
       height: "600px",
       width: "700px",
       data:preCategoryInfo
     });
     dialogRef.componentInstance.lblOperationName = "New Category";
     dialogRef.afterClosed().subscribe(result => {
       this.myTree.FillCategories();
     });
   }
 
}
