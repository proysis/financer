import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { MainComponent } from './main.component';
import { MyTreeComponent } from './my-tree/my-tree.component';
import { TypeTransactionComponent } from './transaction/type-transaction/type-transaction.component';
import { TypeTransactionDayrangeComponent } from './transaction/type-transaction-dayrange/type-transaction-dayrange.component';
import { ReportComponent } from './report/report.component';
import { ListTransactionTemplateComponent } from './transaction/list-transaction-template/list-transaction-template.component';

export const routes: Routes = [
  {path:"main", component:MainComponent, children:[
    {path:"users", component:UsersComponent},
    {path:"my-tree", component:MyTreeComponent},
    {path:"type-transaction", component:TypeTransactionComponent},
    {path:"type-transaction-dayrange", component:TypeTransactionDayrangeComponent},
    {path:"report", component:ReportComponent},
    {path:"list-transaction-template", component:ListTransactionTemplateComponent}
  ]},
];

