import { Component, OnInit, Inject, ViewChild, ElementRef } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";
import { TransactionService } from "src/app/services/transaction-service/transaction.service";
import "../../../../assets/pages/scripts/components-date-time-pickers.js";
import { DatePipe } from "@angular/common";

declare var ComponentsDateTimePickers: any;

@Component({
  selector: "app-update-transaction-template",
  templateUrl: "./update-transaction-template.component.html",
  styleUrls: ["./update-transaction-template.component.css"],
  providers: [DatePipe]
})
export class UpdateTransactionTemplateComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private transactionService: TransactionService,
    private datePipe: DatePipe
  ) {}
  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/]
  };
  transactionTemplate: any = {};
  lblOperationName: string = "";
  typeTransactions: any = [];
  typeTransactionDayRanges: any = [];
  drpTypeTransactionDayRange: any;
  validDates = {};
  startDate:any;
  endDate:any;


  ngOnInit() {
    this.validDates = {};
    ComponentsDateTimePickers.init();
    this.transactionService
      .GetTypeTransaction()
      .subscribe(typeTransactionData => {
        this.typeTransactions = typeTransactionData;
      });
    this.transactionService
      .GetTypeTransactionDayRange()
      .subscribe(typeTransactionDayRangeData => {
        this.typeTransactionDayRanges = typeTransactionDayRangeData;
      });
    if (this.data.id != null || this.data.id !== undefined) {
      debugger;
      this.transactionTemplate = this.data;
      this.startDate = this.datePipe.transform(
        this.transactionTemplate.startdate,
        "MM/dd/yyyy"
      );
      this.endDate = this.datePipe.transform(
        this.transactionTemplate.enddate,
        "MM/dd/yyyy"
      );
    } else {
      this.transactionTemplate.categoryid = this.data.categoryid;
      this.transactionTemplate.personid = this.data.personid;
      this.transactionTemplate.startdate = null;
      this.transactionTemplate.enddate = null;
      this.transactionTemplate.isfixedtransaction = false;
      this.transactionTemplate.date = this.datePipe.transform(
        new Date(),
        "MM/dd/yyyy"
      );
      this.transactionTemplate.lastpaymentdate = null;
      this.transactionTemplate.transactionday = 0;
    }
    this.transactionTemplate.className = "TRANSACTIONTEMPLATE";
  }
  SetTypeTransactionID(event) {
    if (event == 0) {
      this.transactionTemplate.typetransactionid = null;
    } else {
      this.transactionTemplate.typetransactionid = event;
    }
  }

  ngAfterViewInit(){
    this.SetDates();
  }

  SetDates(){
    debugger;
    (<HTMLInputElement>document.getElementById('startDate')).value = this.startDate;
    (<HTMLInputElement>document.getElementById('endDate')).value = this.endDate;
  }

  StartDate() {
    debugger;
    this.transactionTemplate.startdate = (<HTMLInputElement>document.getElementById('startDate')).value;
    
  }
  EndDate() {
    debugger;
    this.transactionTemplate.enddate = (<HTMLInputElement>document.getElementById('endDate')).value != "__/__/____" ? (<HTMLInputElement>document.getElementById('endDate')).value : null;
   
  }
  // LastPaymentDate(){
  //   this.transactionTemplate.lastpaymentdate = (<HTMLInputElement>document.getElementById('lastDate')).value;
  // }

}
