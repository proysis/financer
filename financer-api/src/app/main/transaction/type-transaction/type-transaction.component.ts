import { Component, OnInit, ViewChild } from "@angular/core";
import { BaseServiceService } from "src/app/services/base-service.service";
import {
  MatDialog,
  MatSort,
  MatPaginator,
  MatTableDataSource
} from "@angular/material";
import { TransactionService } from "src/app/services/transaction-service/transaction.service";
import { TypeTransactionUpdateComponent } from "./type-transaction-update/type-transaction-update.component";

@Component({
  selector: "app-type-transaction",
  templateUrl: "./type-transaction.component.html",
  styleUrls: ["./type-transaction.component.css"]
})
export class TypeTransactionComponent implements OnInit {
  displayedColumns: string[] = ["name", "operations"];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  constructor(
    private transactionService: TransactionService,
    private dialog: MatDialog,
    private base: BaseServiceService
  ) {}

  ngOnInit() {
    this.FillList();
  }

  FillList() {
    this.transactionService.GetTypeTransaction().subscribe(data => {
      debugger;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ShowDialog(id: string) {
    if (id != null) {
      this.transactionService.GetTypeTransactionByID(id).subscribe(data => {
        let dialogRef = this.dialog.open(TypeTransactionUpdateComponent, {
          height: "600px",
          width: "700px",
          data: data
        });
        dialogRef.componentInstance.lblTypeTransactionOperationName =
          "Type Transaction Update";
        dialogRef.afterClosed().subscribe(result => {
          this.FillList();
        });
      });
    } else {
      let dialogRef = this.dialog.open(TypeTransactionUpdateComponent, {
        height: "600px",
        width: "700px"
      });
      dialogRef.componentInstance.lblTypeTransactionOperationName =
        "New Type Transaction";
      dialogRef.afterClosed().subscribe(result => {
        this.FillList();
      });
    }
  }
}
