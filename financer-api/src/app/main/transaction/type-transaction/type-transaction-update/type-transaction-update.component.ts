import { Component, OnInit, Inject } from '@angular/core';
import { TransactionService } from 'src/app/services/transaction-service/transaction.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-type-transaction-update',
  templateUrl: './type-transaction-update.component.html',
  styleUrls: ['./type-transaction-update.component.css']
})
export class TypeTransactionUpdateComponent implements OnInit {


  lblTypeTransactionOperationName:string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private transactionService: TransactionService,
    public dialogRef: MatDialogRef<TypeTransactionUpdateComponent>
  ) {}

  typeTransaction: any = {};
  
  ngOnInit() {
    
    if (this.data != null) {
      this.typeTransaction = this.data;
    }
    this.typeTransaction.className = "TYPETRANSACTION";
  }

}
