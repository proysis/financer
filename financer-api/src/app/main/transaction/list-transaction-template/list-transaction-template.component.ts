import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UpdateTransactionTemplateComponent } from '../update-transaction-template/update-transaction-template.component';
import { TransactionService } from 'src/app/services/transaction-service/transaction.service';

@Component({
  selector: 'app-list-transaction-template',
  templateUrl: './list-transaction-template.component.html',
  styleUrls: ['./list-transaction-template.component.css']
})
export class ListTransactionTemplateComponent implements OnInit {
  displayedColumns: string[] = ["name", "description", "date","isfixedtransaction","startdate","enddate", "operations"];
  dataSource: MatTableDataSource<any>;
  transactionTemplate:any = {};
  constructor(private dialog: MatDialog,private transactionTepmlate: TransactionService ) { }
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  onlineuser:any;
  ngOnInit() {
    this.onlineuser = JSON.parse(sessionStorage.getItem("ONLINEUSER"));
    this.FillList();
  }

  public ShowTransactionTemplateDialog(preCategoryInfo: any) {
    let dialogRef = this.dialog.open(UpdateTransactionTemplateComponent, {
      height: "600px",
      width: "700px",
      data: preCategoryInfo
    });
    dialogRef.componentInstance.lblOperationName = "New Transaction Template";
    dialogRef.afterClosed().subscribe(result => {
      this.FillList();
    });
  }

  FillList() {
    this.transactionTepmlate.GetAllTransactionTemplate(this.onlineuser.id).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.transactionTemplate = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
