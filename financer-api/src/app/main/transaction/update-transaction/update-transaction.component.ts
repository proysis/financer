import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { TransactionService } from "src/app/services/transaction-service/transaction.service";
import { AlertifyService } from "src/app/services/alertify-service/alertify.service";
import { BaseServiceService } from "src/app/services/base-service.service";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-update-transaction",
  templateUrl: "./update-transaction.component.html",
  styleUrls: ["./update-transaction.component.css"],
  providers: [DatePipe]
})
export class UpdateTransactionComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private transactionService: TransactionService,
    private alert: AlertifyService,
    private base: BaseServiceService,
    public dialogRef: MatDialogRef<UpdateTransactionComponent>,
    private datePipe: DatePipe
  ) {}
  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/]
  };
  transaction: any = {};
  lblOperationName: string = "";
  typePayments: any = [];
  onThisTerm: Boolean = true;
  validDates = {};
  isFixed = true;
  ngOnInit() {
    debugger;
    this.transactionService.GetTypePayments().subscribe(data => {
      this.typePayments = data;
    });
    if (this.data.transaction !== undefined) {
      this.transaction = this.data.transaction;
    } else {
      this.transaction.transactiontemplateid = this.data.transactiontemplate.id;
    }
    this.transaction.className = "TRANSACTION";
    let date = new Date(this.data.transactiontemplate.startdate);
    date.setDate(date.getDate() + this.data.transactiontemplate.transactionday);
    this.validDates = {};
    if (this.data.transactiontemplate.transactiondayrange == null) {
      switch (this.data.transactiontemplate.typetransactiondayrangeid) {
        case 1:
          this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          for (let index = 0; index < 48; index++) {
            date.setDate(date.getDate() + 7);
            this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          }
          break;
        case 2:
          this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          for (let index = 0; index < 48; index++) {
            date.setMonth(date.getMonth() + 1);
            this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          }
          break;
        case 3:
          this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          for (let index = 0; index < 2; index++) {
            date.setFullYear(date.getFullYear() + 1);
            this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          }
          break;
        case 4:
          this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          for (let index = 0; index < 48; index++) {
            date.setDate(date.getDate() + 15);
            this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
          }
          break;
        default:
          this.isFixed = false;
          break;
      }
    } else {
      this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
      for (let index = 0; index < 48; index++) {
        date.setDate(
          date.getDate() + this.data.transactiontemplate.transactiondayrange
        );
        this.validDates[this.datePipe.transform(date, "MM/dd/yyyy")] = true;
      }
    }
  }

  TransactionDate() {
    this.transaction.transactiondate = (<HTMLInputElement>(
      document.getElementById("transactionDate")
    )).value;
  }

  Update() {
    debugger;
    if (this.onThisTerm) {
      let date = new Date(this.data.transactiontemplate.startdate);
      let newDate = new Date();
      date.setFullYear(newDate.getFullYear());
      date.setMonth(newDate.getMonth());
      date.setDate(
        date.getDate() + parseInt(this.data.transactiontemplate.transactionday)
      );
      this.transaction.transactiondate = this.datePipe.transform(
        date,
        "MM/dd/yyyy"
      );
    }
    this.base.Update(this.transaction).subscribe(
      data => {
        this.alert.success("İşleminiz başarıyla gerçekleşti");

        this.dialogRef.close("closed");
      },
      err => {
        this.alert.error("İşlem esnasında bir hata ile karşılaşıldı.");
      }
    );
  }

  FilterDate = (d: Date): boolean => {
    if(this.isFixed){
      return this.validDates[this.datePipe.transform(d, "MM/dd/yyyy")];
    }else{
      return true;
    }
  };
}
