import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { TransactionService } from 'src/app/services/transaction-service/transaction.service';
import { TypeTransactionUpdateComponent } from '../type-transaction/type-transaction-update/type-transaction-update.component';
import { TypeTransactionDayrangeUpdateComponent } from './type-transaction-dayrange-update/type-transaction-dayrange-update.component';

@Component({
  selector: 'app-type-transaction-dayrange',
  templateUrl: './type-transaction-dayrange.component.html',
  styleUrls: ['./type-transaction-dayrange.component.css']
})
export class TypeTransactionDayrangeComponent implements OnInit {
  displayedColumns: string[] = ["name", "dayrange", "operations"];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  constructor(
    private transactionService: TransactionService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.FillList();
  }

  FillList() {
    this.transactionService.GetTypeTransactionDayRange().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ShowDialog(id: string) {
    if (id != null) {
      this.transactionService.GetTypeTransactionDayRangeByID(id).subscribe(data => {
        let dialogRef = this.dialog.open(TypeTransactionDayrangeUpdateComponent, {
          height: "600px",
          width: "700px",
          data: data
        });
        dialogRef.componentInstance.lblTypeTransactionDayrangeOperationName =
          "Type Transaction Day Range Update";
        dialogRef.afterClosed().subscribe(result => {
          this.FillList();
        });
      });
    } else {
      let dialogRef = this.dialog.open(TypeTransactionDayrangeUpdateComponent, {
        height: "600px",
        width: "700px"
      });
      dialogRef.componentInstance.lblTypeTransactionDayrangeOperationName =
        "New Type Transaction Day Range";
      dialogRef.afterClosed().subscribe(result => {
        this.FillList();
      });
    }
  }
}
