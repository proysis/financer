import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TransactionService } from 'src/app/services/transaction-service/transaction.service';

@Component({
  selector: 'app-type-transaction-dayrange-update',
  templateUrl: './type-transaction-dayrange-update.component.html',
  styleUrls: ['./type-transaction-dayrange-update.component.css']
})
export class TypeTransactionDayrangeUpdateComponent implements OnInit {

  lblTypeTransactionDayrangeOperationName:string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private transactionService: TransactionService,
    public dialogRef: MatDialogRef<TypeTransactionDayrangeUpdateComponent>
  ) {}

  typeTransactionDayrange: any = {};
  
  ngOnInit() {
    
    if (this.data != null) {
      this.typeTransactionDayrange = this.data;
    }
    this.typeTransactionDayrange.className = "TYPETRANSACTIONDAYRANGE";
  }
}
