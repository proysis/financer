/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TypeTransactionDayrangeUpdateComponent } from './type-transaction-dayrange-update.component';

describe('TypeTransactionDayrangeUpdateComponent', () => {
  let component: TypeTransactionDayrangeUpdateComponent;
  let fixture: ComponentFixture<TypeTransactionDayrangeUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeTransactionDayrangeUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeTransactionDayrangeUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
