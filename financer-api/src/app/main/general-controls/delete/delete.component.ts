import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseServiceService } from 'src/app/services/base-service.service';
import { AlertifyService } from 'src/app/services/alertify-service/alertify.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
  @Input() value:any = {};
  @Input() className:any;
  @Input() childValueClassName:any;
  @Output()
  fillList = new EventEmitter();

  constructor(private base : BaseServiceService, protected alert: AlertifyService) { }
 
  ngOnInit() {
  }

  Delete() {
    debugger;
    if(this.className !== undefined){
      this.value.className = this.className;
    }
    debugger;
    if(this.childValueClassName !== undefined){
      this.value.childValueClassName = this.childValueClassName;
    }
    this.base.Delete(this.value).subscribe(
      data => {
        this.alert.success("Silme işleminiz başarıyla gerçekleşti");
        this.fillList.emit()
      },
      err => {
        this.alert.error(
          "İşlem esnasında bir hata ile karşılaşıldı. Hata : " + err
        );
      }
    );;
  }
}
