import { Component, OnInit, Input } from '@angular/core';
import { BaseServiceService } from 'src/app/services/base-service.service';
import { AlertifyService } from 'src/app/services/alertify-service/alertify.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  @Input() value:any = {};
  @Input() dialog:any = null;
  @Input() form:any;

  constructor(private base : BaseServiceService, protected alert: AlertifyService) { }

  ngOnInit() {
  }

  Update() {
    this.base.Update(this.value).subscribe(
      data => {
        this.alert.success("Action successfuly");
        if(this.dialog != null){
          this.dialog.close("closed");
        }
      },
      err => {
        this.alert.error(
          "An error was encountered during the process. Error :" + err
        );
      }
    );;
    
  }

}
