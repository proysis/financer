import { Component, OnInit } from '@angular/core';

import "../../assets/layouts/layout/scripts/layout.js";
import "../../assets/layouts/layout/scripts/demo.min.js";
import "../../assets/layouts/global/scripts/quick-sidebar.min.js";


declare var Layout : any;
declare var Demo : any;
declare var QuickSidebar : any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() {

    
   }

  ngOnInit() {
    
  }
  ngAfterViewInit(){
    Layout.init();
    Demo.init();
    QuickSidebar.init();
  }
}
