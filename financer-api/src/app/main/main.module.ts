import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule, DatePipe } from "@angular/common";
import { routes } from "./main.routing";
import { UsersComponent } from "./users/users.component";

import { UserControlsModule } from "./user-controls/user-controls.module";
import { SidebarComponent } from "./user-controls/sidebar/sidebar.component";
import { NavbarComponent } from "./user-controls/navbar/navbar.component";
import { FooterComponent } from "./user-controls/footer/footer.component";
import { MyTreeComponent } from "./my-tree/my-tree.component";
import {
  MatButtonModule,
  MatRippleModule,
  MatInputModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  DateAdapter,
  MAT_DATE_LOCALE,
  MatTreeModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatCardModule
} from "@angular/material";
import { MatIconModule } from "@angular/material/icon";
// import { FlexLayoutModule } from '@angular/flex-layout';
import { UserUpdateComponent } from "./users/user-update/user-update.component";
import { FormsModule } from "@angular/forms";
import { TextMaskModule } from 'angular2-text-mask';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { BaseServiceService } from "../services/base-service.service";
import { UpdateComponent } from "./general-controls/update/update.component";
import { DeleteComponent } from "./general-controls/delete/delete.component";
import { TreeViewComponent } from "./my-tree/tree-view/tree-view.component";
import { NestableModule } from "ngx-nestable";
import { UpdateTreeComponent } from "./my-tree/update-tree/update-tree.component";
import { ActionsComponent } from "./my-tree/actions/actions.component";
import { TypeTransactionUpdateComponent } from "./transaction/type-transaction/type-transaction-update/type-transaction-update.component";
import { TypeTransactionComponent } from "./transaction/type-transaction/type-transaction.component";
import { TypeTransactionDayrangeUpdateComponent } from "./transaction/type-transaction-dayrange/type-transaction-dayrange-update/type-transaction-dayrange-update.component";
import { TypeTransactionDayrangeComponent } from "./transaction/type-transaction-dayrange/type-transaction-dayrange.component";
import { UpdateTransactionTemplateComponent } from "./transaction/update-transaction-template/update-transaction-template.component";
import { IsSelectedPipe } from "../pipes/IsSelected.pipe";
import { FilterByIDPipe } from "../pipes/filterByID.pipe";
import { UpdateTransactionComponent } from "./transaction/update-transaction/update-transaction.component";
import { FilterByTemplateIDPipe } from "../pipes/filterByTemplateID.pipe";
import { ReportComponent } from "./report/report.component";
import { ListTransactionTemplateComponent } from "./transaction/list-transaction-template/list-transaction-template.component";
import { DrpCategoryComponent } from "./report/drp-category/drp-category.component";
import { UserService } from "../services/user-service/user.service";
import { TransactionService } from "../services/transaction-service/transaction.service";
import { LoginService } from "../services/login-service/login.service";
import { CategoryService } from "../services/category-service/category.service";
import { OnlyNumericDirective } from "../directives/OnlyNumeric.directive";


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}),
    UserControlsModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatInputModule,
    MatTooltipModule,
    // FlexLayoutModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSortModule,
    MatIconModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    TextMaskModule
  ],
  declarations: [
    UsersComponent,
    MyTreeComponent,
    UserUpdateComponent,
    UpdateTreeComponent,
    UpdateComponent,
    DeleteComponent,
    TreeViewComponent,
    ActionsComponent,
    TypeTransactionUpdateComponent,
    TypeTransactionComponent,
    TypeTransactionDayrangeComponent,
    TypeTransactionDayrangeUpdateComponent,
    UpdateTransactionTemplateComponent,
    IsSelectedPipe,
    FilterByIDPipe,
    FilterByTemplateIDPipe,
    UpdateTransactionComponent,
    ReportComponent,
    ListTransactionTemplateComponent,
    DrpCategoryComponent,
    OnlyNumericDirective
  ],
  exports: [SidebarComponent, NavbarComponent, FooterComponent],
  entryComponents: [
    UserUpdateComponent,
    UpdateTreeComponent,
    TypeTransactionUpdateComponent,
    UpdateTransactionTemplateComponent,
    TypeTransactionDayrangeUpdateComponent,
    UpdateTransactionComponent
  ],
  providers: [BaseServiceService, UserService, TransactionService, CategoryService, { provide: MAT_DATE_LOCALE, useValue: "en" }]
})
export class MainModule {}
