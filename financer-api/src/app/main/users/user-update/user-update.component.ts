import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, DateAdapter } from "@angular/material";
import { UserService } from "../../../services/user-service/user.service";

@Component({
  selector: "app-user-update",
  templateUrl: "./user-update.component.html",
  styleUrls: ["./user-update.component.css"]
})
export class UserUpdateComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    public dialogRef: MatDialogRef<UserUpdateComponent>,
    private adapter: DateAdapter<any>
  ) {}
  public mask = {
    guide: true,
    showMask : true,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/',/\d/, /\d/, /\d/, /\d/]
  };
  lblUserOperationName: string = "";
  passwordConfirm: string = "";
  user: any = {};
  dateEvent: any;
  
  ngOnInit() {
    
    if (this.data != null) {
      this.user = this.data;
      this.dateEvent = this.data.createdate;
      this.passwordConfirm = this.data.password;
    }
    this.user.className = "PERSON";
  }
  date(){
    this.user.createdate = (<HTMLInputElement>document.getElementById('createDate')).value;
  }
}
  