import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from "@angular/material";
import { UserService } from "../../services/user-service/user.service";
import { UserUpdateComponent } from "./user-update/user-update.component";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ["name", "surname", "createdate", "operations"];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  constructor(
    private userService: UserService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.FillList();
  }

  FillList() {
    this.userService.GetUser().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ShowDialog(id: string) {
    if (id != null) {
      this.userService.GetUserByID(id).subscribe(data => {
        let dialogRef = this.dialog.open(UserUpdateComponent, {
          height: "600px",
          width: "700px",
          data: data
        });
        dialogRef.componentInstance.lblUserOperationName = "User Update";
        dialogRef.afterClosed().subscribe(result => {
          this.FillList();
        });
      });
    } else {
      let dialogRef = this.dialog.open(UserUpdateComponent, {
        height: "600px",
        width: "700px"
      });
      dialogRef.componentInstance.lblUserOperationName = "New User";
      dialogRef.afterClosed().subscribe(result => {
        this.FillList();
      });
    }
  }
}