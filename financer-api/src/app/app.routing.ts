import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { MainComponent } from "./main/main.component";
// import { UsersComponent } from "./main/users/users.component";

export const mainRoutes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "main",
    component: MainComponent
  },
  { path: "**", redirectTo:"login",pathMatch:"full" }
  
];
