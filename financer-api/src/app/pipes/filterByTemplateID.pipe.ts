import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByTemplateID'
})
export class FilterByTemplateIDPipe implements PipeTransform {

  transform(value: any[], id: any): any {
    if(id){
      return value.filter(x=>x.transactiontemplateid == id);
    }
    return value;
  }

}
