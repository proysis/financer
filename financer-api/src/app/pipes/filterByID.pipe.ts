import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByID'
})
export class FilterByIDPipe implements PipeTransform {

  transform(value: any[], id: any): any {
    if(id){
      return value.filter(x=>x.categoryid == id);
    }
    return value;
  }

}
