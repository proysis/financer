import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'IsSelected'
})
export class IsSelectedPipe implements PipeTransform {

  transform(value: any): any {
    if(value == 0){
      return null;
    }
    return value;
  }

}
